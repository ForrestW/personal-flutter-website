

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/////////////////////////////////////////
///   
/// Text Styling
///
/////////////////////////////////////////
final TextStyle header1 = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 35,
  color: Colors.blue[700]
);

final TextStyle header2 = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 27,
  color: Colors.blue[700]
);

final TextStyle header3 = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 23,
  color: Colors.blue[700]
);

final TextStyle footer1 = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 30,
  color: Colors.white
);

final TextStyle basicText = TextStyle(
  fontSize: 23
);

final TextStyle basicTextBold = TextStyle(
  fontSize: 23,
  fontWeight: FontWeight.bold
);

final TextStyle basicTextItalics = TextStyle(
  fontSize: 23,
  fontStyle: FontStyle.italic
);

final TextStyle basicFooterText = TextStyle(
  fontSize: 20,
  color: Colors.white
);

final TextStyle subHead = TextStyle(
  fontSize: 26,
  color: Colors.white
);


////////////////////////////////////////
///
/// Colors
///
////////////////////////////////////////
final blue = Colors.blue;
final white = Colors.white;
final black = Colors.black;

final appbarColors = black;


AppBarTheme defaultTheme = AppBarTheme(
  color: appbarColors,
);

