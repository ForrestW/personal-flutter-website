import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:the_basics/themes.dart';
import 'package:the_basics/toolbox.dart';
import 'package:flutter/widgets.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: appbarColors,
      
      title: 'Forrest Walsh',
      theme: ThemeData(
        appBarTheme: defaultTheme,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Basic Website'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        
        backgroundColor: Colors.white,
        appBar: generateHeader(widget.title),

        body: SingleChildScrollView( child: Card(
            margin: EdgeInsets.fromLTRB(55, 0, 55, 0),
            child:Column(
          children: <Widget> [

            introductionCard(),

            aboutMeTile(),

            educationTile(),

            computingTile(),

            programmingProjects(),

            artworkTile(),

            linksTile()

          ]
        )
        ),
      ),

      bottomNavigationBar: siteFooter()
      
    );
  }
}


///
/// Main heading for the site
///
Container siteHeader(String title) {
  return Container(
    child: Row(
      

      children:[
        leading(),
        padItHorizontal(20),
        Column(
      children: [
        // Main Header
        Text(
          "Forrest Bracken Walsh",
          style: header1,
        ),

        // SubHead
        Text(
          "Software Engineer and Artist",
          style: subHead,
        ),
      ],
    ),
      ]
    )
  );
}


// Footer for the site
Container siteFooter() {
  return Container(
    height: 110,
    color: appbarColors,

    child: Column(
      children: <Widget> [
        padItVertical(20),
        // Header
        Row(
          children: [
            padItHorizontal(20),
            Text("Contact Info:", style: header1,)
          ],
        ),

        padItVertical(15),

        // Info
        Row(
          children: [
            padItHorizontal(20),
              
            Text("Brackw@live.com", style: basicFooterText,),

            padItHorizontal(10),

            Text("912-212-6600", style: basicFooterText,)
          ]
        ),

        padItVertical(10),


      ]
    ),
  );
}

///
/// Page Headers
///
AppBar generateHeader(String pageTitle) {
  return AppBar(
      title: siteHeader(pageTitle),
      toolbarHeight: 150,
  );
}

Widget leading() {
  return Container(
    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
    child: Image.network("https://scontent.fdtw2-1.fna.fbcdn.net/v/t1.0-9/50755893_2658351620856581_4548321756024143872_n.jpg?_nc_cat=111&_nc_sid=85a577&_nc_ohc=AR-nYU43v1IAX--PmaQ&_nc_ht=scontent.fdtw2-1.fna&oh=5e76741d822d681016515324841d5dc7&oe=5F3F7190",
     fit: BoxFit.none,),
  );
}


///
/// About Me Expansion Tile
///
ExpansionTile aboutMeTile() {
  return ExpansionTile(
    
    title: Text("About Me:", style: header1,),
    children: [
       // About Me Card
          Container(
            margin: EdgeInsets.fromLTRB(55, 0, 55, 0),

            child: Column(
              children: [
                padItVertical(20),

                Text("I was born on July 18, 1996 in Bristol Tennessee. Computing became a passion for me at a young age through my love of gaming. In high school, I became intersted in building computers and was introduced to programming. My introduction to programming wasn't the best, but that did not stop me from trying again in college. As of now, I am a graduate of Bowling Green State University with a Bachelors of Science in Software Engineering.",
                style:basicText
                ),

                padItVertical(20)
              ]
            ),

          )
    ],
    );
}

ExpansionTile computingTile() {
  return ExpansionTile(
    title: Text("Computer Building:", style: header1),
          // Computer Building 
    children:[  
      Container(
        margin:  EdgeInsets.fromLTRB(55, 0, 55, 0),
        child: Column(
          children:<Widget> [
            padItVertical(20),

            Text("I learned how to build computers in my junior year of highschool, I was taught by a good friend of mine. The first full build I did was in 2014 when I moved all my computer parts to a much bigger case. Until the summer of 2019 I had just been upgrading my older parts.",
            style: basicText,),

            padItVertical(20),

            Container(
              child:
                Row(
                  children:[
                    // List of Parts
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        bulletPoint("Processor:", " AMD Phenom II"),

                        bulletPoint("GPU:", " Asus GTX 550 Ti"),

                        bulletPoint("Motherboard:", " ASRock 970 Extreme 3"),

                        bulletPoint("Memory:", " 8 GB"),

                        bulletPoint("Storage:", " 1 TB Western Digital Hard Drive")
                      ],
                    ),

                    padItHorizontal(20),

                    // Image
                    imageContainer('http://forrestbwalsh.com/images/PC2014.jpg')
                    //Image.network('http://forrestbwalsh.com/images/PC2014.jpg')
                  ]
                ),
            ),

            padItVertical(20),

            Text("The Computer above gradually changed over the years with two GPU upgrades, a RAM increase, and an upgraded processor. In the summer of 2019 I completely rebuilt my computer due to a change in AMD processors, this build contained:", style: basicText,),

            padItVertical(20),

            Container(
              child:Row(
                children:[
                  
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children:[

                      bulletPoint("Processor:", " AMD Ryzen 7 2700"),

                      bulletPoint("GPU:", " Asus GTX 1070"),

                      bulletPoint("Motherboard:", " MSI"),

                      bulletPoint("Memory:", " 16 GB"),

                      bulletPoint("Storage:", " 250 GB M.2 Solid State Drive"),

                      bulletPoint("Storage Ctd:", " 4TB Western Digital Black HDD"),

                      bulletPoint("Storage Ctd:", "1TB Western Digital Drive")
                    
              
                  ]),

                  padItHorizontal(30),

                  imageContainer('http://forrestbwalsh.com/images/PC2019.jpg')
                  //Image.network('http://forrestbwalsh.com/images/PC2019.jpg')
                ]
              )
            ),

            padItVertical(20),

            Text("I find building computers to be an interesting task and one that is very satisfying upon completion. Somedays I feel very experienced in builing computers however I have a lot more that I can learn, such as watercooling, overclocking, and Intel chipsets.", style: basicText,),

            padItVertical(20)
          ]
        ),
      ),
    ]
  );
}

ExpansionTile artworkTile() {
  return ExpansionTile( title: Text("Artwork:", style: header1,),

    // Content
    children: [
      Text("Drawings", style: header2),
      Text("Most of my drawings are done in pencil on 18x24 paper. I've gradually moved over to digital drawing however the transition has not been easy.", style: basicText),

      padItVertical(20),

      GridView.count(
        shrinkWrap: true,
        crossAxisCount: 3,
        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
        mainAxisSpacing: 20,
        crossAxisSpacing: 300,
        childAspectRatio: 1.25,
        

        children: [
          GestureDetector(
            child:Container( child:imageContainer('http://forrestbwalsh.com/images/Spring_2017/Crow_Edited_small.jpg')),
            //Image.network('http://forrestbwalsh.com/images/Spring_2017/Crow_Edited_small.jpg'),
            onTap: () {launch("http://forrestbwalsh.com/images/Spring_2017/Crow_Edited.jpg"); },
          ),
          
          GestureDetector(
            child:Container( child:imageContainer('http://forrestbwalsh.com/images/Spring_2017/Berserk_Red_Edit_Small.jpg')),
            //Image.network('http://forrestbwalsh.com/images/Spring_2017/Berserk_Red_Edit_Small.jpg'),
            onTap: () {launch("http://forrestbwalsh.com/images/Spring_2017/Berserk_Red_Edit.jpg"); },
          ),

          GestureDetector(
            child: Container(child:imageContainer('http://forrestbwalsh.com/images/Spring_2018/NCR_Ranger_Edited_small.jpg')),
            //Image.network('http://forrestbwalsh.com/images/Spring_2018/NCR_Ranger_Edited_small.jpg'),
            onTap: () {launch("http://forrestbwalsh.com/images/Spring_2018/NCR_Ranger_Edited.jpg"); },
          ),
          
          GestureDetector(
            child: Container(child:imageContainer('http://forrestbwalsh.com/images/Spring_2018/Fluted_Edited_small.jpg')),
            //Image.network('http://forrestbwalsh.com/images/Spring_2018/Fluted_Edited_small.jpg'),
            onTap: () {launch("http://forrestbwalsh.com/images/Spring_2018/Fluted_Edited.jpg"); },
          ),
          
          GestureDetector(
            child: Container(child:imageContainer('http://forrestbwalsh.com/images/Winter_2020/Mountains_edited_small.jpg')),
             //Image.network('http://forrestbwalsh.com/images/Winter_2020/Mountains_edited_small.jpg'),
            onTap: () {launch('http://forrestbwalsh.com/images/Winter_2020/Mountains_edited.jpg');},
          ),

          GestureDetector(
            child: Container(child:imageContainer('http://forrestbwalsh.com/images/Winter_2020/Stalker_default_small.jpg')),
            //Image.network('http://forrestbwalsh.com/images/Winter_2020/Stalker_default_small.jpg', fit: BoxFit.none,),
            onTap: () {launch('http://forrestbwalsh.com/images/Winter_2020/Stalker_default.jpg');},
          ),
          
          GestureDetector(
            child:Container( child:imageContainer('http://forrestbwalsh.com/images/Winter_2019/Stalker_Edited_Small.jpg')),
            //Image.network('http://forrestbwalsh.com/images/Winter_2019/Stalker_Edited_Small.jpg'),
            onTap: () {launch('http://forrestbwalsh.com/images/Winter_2019/Stalker_Edited.jpg');},
          ),
          
          GestureDetector(
            child: Container(child:imageContainer('http://forrestbwalsh.com/images/Summer_2019/Sekiro_small.jpg'))
            //Image.network('http://forrestbwalsh.com/images/Summer_2019/Sekiro_small.jpg'),
            //onTap: () {launch('');},
          ),

          GestureDetector(
            child: Container(child:imageContainer('http://forrestbwalsh.com/images/Spring_2018/Oscar_Edited_small.jpg')),
            //Image.network('http://forrestbwalsh.com/images/Spring_2018/Oscar_Edited_small.jpg'),
            onTap: () {launch('http://forrestbwalsh.com/images/Spring_2018/Oscar_Edited.jpg');},
          ),
          
          GestureDetector(
            child: Container(child:imageContainer('http://forrestbwalsh.com/images/Fall_2018/Josh_Edit_Small.png'))
            //Image.network('http://forrestbwalsh.com/images/Fall_2018/Josh_Edit_Small.png'),
            //onTap: () {launch('');},
          ),

          GestureDetector(
            child: Container(child:imageContainer('http://forrestbwalsh.com/images/Winter_2020/Cinder_Small.png')),
            //Image.network('http://forrestbwalsh.com/images/Winter_2020/Cinder_Small.png'),
            onTap: () {launch('http://forrestbwalsh.com/images/Winter_2020/Cinder_Big.jpg');},
          ),

          GestureDetector(
            child: Container(child:imageContainer('http://forrestbwalsh.com/images/Digital_Art/CLogoEdit_small.jpg')),
             //Image.network('http://forrestbwalsh.com/images/Digital_Art/CLogoEdit_small.jpg'),
            onTap: () {launch('http://forrestbwalsh.com/images/Digital_Art/CLogoEdit.jpg');},
          ),
        
        ],
      ),
    ],  
  );
}

ExpansionTile educationTile() {
  return ExpansionTile(
    // Label
    title: Text("Education:", style: header1),

    // Content
    children: [
      Container(
        margin:  EdgeInsets.fromLTRB(55, 0, 55, 0),

        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget> [
            Text("Bowling Green State University", style: header3,),
            Text("I am a graduate from Bowling Green State University with a Bachelors Degree in Software Engineering in August 2020. The majority of my programming knowledge comes from my coursework at Bowling Green State University.", style: basicText,),
            padItVertical(30),
            Text("Programming Languages", style: header3,),
            FittedBox(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children:<Widget> [

                  bulletPoint("C++:", "C++ was the first language that I learned at BGSU."),

                  bulletPoint("C#:", " I used C# at BGSU to create basic Windows applications. I find this language very useful for building applications with basic GUIs. "),
                  
                  bulletPoint("Python:", " Python is the second language that I experienced while at BGSU."),
                  
                  bulletPoint("HTML/JavaScript/CSS:", " I learned web development with JavaScript while at BGSU. My first website was made using what I learned from that course."),
                  
                  bulletPoint("SQL", " I learned how to create and manage relational databases while at BGSU. We were shown how to work with SQL in command line as well as tools like MySQL Workbench."),
                  
                  bulletPoint("Dart:", " I gained experience using dart through the Townie Cup project that I worked on for my internship requirement at BGSU. Although I just learned this language it has quickly become one of my favorites.")
                  
                ]
              )
            ),

            padItVertical(30),
            Text("Currently Studying", style: header3,),
            Text("I am currently learning more about Flutter, React, Unreal Engine, and Japanese.", style: basicText),

            padItVertical(30)
          ]
        ),
      )
    ],
  );
}

///
/// Tile for links
///
ExpansionTile linksTile() {
  return ExpansionTile(
    title: Text("Other Links:", style: header1,),
    initiallyExpanded: true,
    children: [
        // Buttons
        Row(
          children: [
            padItHorizontal(15),
            
            // Resume Button
            FlatButton(
              color: blue,
              textColor: white,

              onPressed: () {launch('http://forrestbwalsh.com/documents/FBWSummer2020.pdf');},
              child: Text("Resume", style: basicText,)
            ),

            padItHorizontal(15),

            // Github button
            FlatButton(
              color: blue,
              textColor: white,

              onPressed: () {launch("https://github.com/Fwalsh96");}, 
              child: Text("GitHub", style: basicText,)
            ),

            padItHorizontal(15),

            // Gitlab button
            FlatButton(
              color: blue,
              textColor: white,

              onPressed: () {launch('https://gitlab.com/ForrestW');}, 
              child: Text("GitLab", style: basicText,)
            ),

            padItHorizontal(15),

            // Art Prints button
            FlatButton(
              color: blue,
              textColor: white,
              
              onPressed: () {launch("https://www.redbubble.com/people/Brackzillar/shop?asc=u&ref=account-nav-dropdown#profile");}, 
              child: Text("Art Prints", style: basicText,)
            ),

            padItHorizontal(15),

            // Previous Website button
            FlatButton(
              color: blue,
              textColor: white,

              child: Text("Previous Website", style: basicText,),
              onPressed: () {launch('http://forrestbwalsh.com/oldsite.htm');},
            )
          ],
        )
    ],
  );
}

///
/// Introduction Expansion TIle
///
ExpansionTile introductionCard() {
  return ExpansionTile(
      title: Text("Introduction: ", textAlign: TextAlign.left, style: header1 ),
      initiallyExpanded: true,

      children: [
        Container( 
          margin: EdgeInsets.fromLTRB(55, 0, 55, 0),
          child: Column(
          children: [
              padItVertical(15),

              // Heading 1 Content
              Text("Hello, my name is Forrest Walsh and this is my personal website. I upload artwork, programming projects, and other information about myself. This website was created by myself using Flutter, and is powered by Amazon Web Services.",
                style: basicText,
                      
              ),

              padItVertical(15)
          ],
        ),
      )
    ],
  );
}

///
/// Expansion tile for programming projects
///
ExpansionTile programmingProjects() {
  return ExpansionTile(
    title: Text("Programming Projects: ", style: header1),
    
    
    children: [
      Container( 
        margin: EdgeInsets.fromLTRB(55, 0, 55, 0),
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          padItVertical(30),

          // Status Monitor
          Text("Status Monitor", style: header3),
          Row(children: [
            // Status Monitor
            Text("I created the webpage below as a status monitor for a SC-FI ", style: basicText,),
            Text("Dungeons and Dragons", style: basicTextItalics,),
            Text(" campaign. I plan to make a more advanced version of this project in the future.", style: basicText,)
          ],),
        
          Image.network("http://forrestbwalsh.com/images/HtmlWork.gif", fit: BoxFit.contain,),
          
          padItVertical(30),

          // Townie Cup project
          Text("Townie Cup Project", style: header3),
          Text("Worked on a small team to combine the IOS and Android versions of the Townie Cup app into a single Flutter App.", style: basicText,),

          Image.network("https://s3.us-east-2.amazonaws.com/forrestbwalsh.com/images/TownieCupVideo.gif", ),

          padItVertical(30)
        ],
      ))
    ],
  );
}

///
/// Text element that has a bold heading and basic text
///
Row bulletPoint(String heading, String body) {
  return Row(
    children: [
      Text(heading, style:basicTextBold),
      Text(body, style: basicText,)
    ],
  );
}

///
/// Appbar meant to resemble previous site
///
AppBar alternateAppbar(String title) {
  return AppBar(
        title: siteHeader(title),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/header.jpg"),
              fit: BoxFit.cover,
            )
          )
        )
  );
}

///
/// Function that returns a container of an image
/// 
Container imageContainer(String link) {
  return Container(
    padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
    color: blue,
    child: Image.network(link),
  );
}

class SizeConfig {
  static MediaQueryData mediaData;
  static double screenWidth;
  static double screenHeight;
  static double blockSizeHorizontal;
  static double blockSizeVertical;

  void init(BuildContext context) {
    mediaData = MediaQuery.of(context);
    screenWidth = mediaData.size.width;
    screenHeight = mediaData.size.height;
    blockSizeHorizontal = screenWidth/100;
    blockSizeVertical = screenWidth/100;
  }
}


bool useMobileLayout(BuildContext context) {
  var shortestSide = MediaQuery.of(context).size.shortestSide;

  if(shortestSide < 600) {
    return true;
  } else {
    return false;
  }
}