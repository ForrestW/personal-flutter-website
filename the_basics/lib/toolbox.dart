import 'package:flutter/material.dart';

/// ===============================================
///                   Padding
/// ===============================================
/// Function that can be used to add padding to Columns.
/// Use a number as a parameter to change the size of the padding.
SizedBox padItVertical(double number)
{
  return SizedBox(
    height: number,
  );
}

SizedBox padItHorizontal(double number)
{
  return SizedBox(
    width: number,
  );
}